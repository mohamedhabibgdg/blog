<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Comment;
use Faker\Generator as Faker;

$factory->define(Comment::class, function (Faker $faker) {
    return [
        'comment'=>$faker->sentences(rand(10,20),true),
        'user_id'=>\App\User::all()->random()->id,
        'commentable_type'=>\App\Article::class,
        'commentable_id'=>\App\Article::all()->random()->id,
    ];
});
