<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Article;
use App\ArticleTag;
use App\Tag;
use Faker\Generator as Faker;

$factory->define(ArticleTag::class, function (Faker $faker) {
    return [
        'article_id'=> Article::all()->random()->id,
        'tag_id'=> Tag::all()->random()->id,
    ];
});
