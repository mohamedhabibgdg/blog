<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Article;
use Faker\Generator as Faker;

$factory->define(Article::class, function (Faker $faker) {

    return [
        'title'=>$faker->words(rand(3,7),true),
        'body'=>$faker->sentences(rand(3,7),true),
        'user_id'=>\App\User::all()->random()->id,
    ];
});
