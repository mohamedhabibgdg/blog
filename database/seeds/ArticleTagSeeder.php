<?php

use Illuminate\Database\Seeder;

class ArticleTagSeeder extends Seeder{

    public function run()
    {
        factory(\App\ArticleTag::class,50)->create();

    }
}
