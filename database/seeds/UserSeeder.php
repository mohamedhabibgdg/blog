<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder{

    public function run(){
        factory(\App\User::class,50)->create()->each(function ($user){
            $this->command->info($user->email);
        });

    }
}
