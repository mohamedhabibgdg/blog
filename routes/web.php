<?php

use Illuminate\Support\Facades\Route;
Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
Route::middleware('auth')->group(function () {
    Route::get('/home', 'HomeController@index')->name('home');
    Route::resource('articles', 'ArticleController');
    Route::post('articles/{article}/{comment?}', 'ArticleController@storeComment')->name('articles.create.comment');
    Route::get('/my/friends','FriendController@index')->name('friends');
    Route::get('/request/friends','FriendController@request')->name('request.friends');
    Route::get('/user/{user}','FriendController@show')->name('user.show');

    Route::post('/friends/{user}','FriendController@store')->name('friends.request');
    Route::post('/friend/{friend}/{action}','FriendController@update')->name('friends.response');
});
