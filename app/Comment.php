<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model{
    protected $guarded=[''];

    public function commentable(){ return $this->morphTo(); }

    public function parent(){ return $this->belongsTo(self::class,'comment_id');}
    public function user(){ return $this->belongsTo(User::class,'user_id');}

    public function childs(){
        return $this->hasMany(self::class,'comment_id');
    }
    protected static function boot(){
        parent::boot();
        static::creating(function ($comment) {
            $comment->user_id= $comment->user_id ??  auth()->id();
        });
    }
}
