<?php

namespace App\Http\Controllers;

use App\Article;
use App\Comment;
use App\Tag;
use foo\bar;
use Illuminate\Http\Request;

class ArticleController extends Controller{

    public function index(){
        $articles=Article::paginate();
        return view('article.index',[
            'articles'=>$articles
        ]);
    }

    public function create(){
        return  view('article.create');
    }

    public function store(Request $request){
        $validated=$request->validate([
            'title'=>['required','min:3'],
            'body'=>['required'],
            'tags'=>['required','array'],
        ]);
        $tags=[];
        foreach ($validated['tags'] as $tag) $tags[]=Tag::firstOrCreate(['name'=>$tag])->id;
//        dd($tags);
        unset($validated['tags']);
        $article=Article::create($validated);

        $article->tags()->sync($tags);

        return back();
    }

    public function storeComment(Article $article,Comment $comment,Request $request){
        $validated=$request->validate([ 'comment'=>['required'] ]);
        if (!$comment->id){
            $article->comments()->create($validated);
        }else{
            $comment->childs()->create([
                'comment'=>$validated['comment'],
                'commentable_type'=>$comment->commentable_type,
                'commentable_id'=>$comment->commentable_id,
            ]);
        }
        return back();
    }

    public function show(Article $article){

        return view('article.show',compact('article'));
    }

    public function edit(Article $article){

        return view('article.edit',compact('article'));
    }
    public function update(Request $request, Article $article){
        $validated=$request->validate([
            'title'=>['sometimes','min:3'],
            'body'=>['sometimes'],
            'tags'=>['sometimes','array'],
        ]);
        if (count($validated)>0){
            $article->update($validated);
            if (count($validated['tags'])>0){
                $tags=[];
                foreach ($validated['tags'] as $tag) $tags[]=Tag::firstOrCreate(['name'=>$tag])->id;
                $article->tags()->sync($tags);
            }
        }
        return back();
    }

    public function destroy(Article $article){
       $article->delete();

        return  back();
    }
}
