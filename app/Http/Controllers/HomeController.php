<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class HomeController extends Controller{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        $users=User::where('id','<>',auth()->id())->paginate();
        return view('home',compact('users'));
    }
}
