<?php

namespace App\Http\Controllers;

use App\Comment;
use Illuminate\Http\Request;

class CommentController extends Controller{

    public function update(Request $request, Comment $comment){
        $validated=$request->validate([ 'comment'=>['required'] ]);
        $comment->update($validated);
        return back();
    }

    public function destroy(Comment $comment){
        $comment->delete();
        return back();

    }
}
