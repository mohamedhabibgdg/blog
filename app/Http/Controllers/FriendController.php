<?php

namespace App\Http\Controllers;

use App\Friend;
use App\User;
use Illuminate\Http\Request;

class FriendController extends Controller{

    public function index(){
        $friends=auth()->user()->myFriends()->paginate();
        return view('friend.all',compact('friends'));
    }

    public function show(User $user){
        $isFriend=Friend::IsFriend($user->id)->count() ===0 ;
        $hasRequest=Friend::HasRequest($user->id)->count() === 1 ;
        return view('friend.user',compact('user','isFriend','hasRequest'));
    }

    public function request(){
        // request friends
        $friends=auth()->user()->requests()->paginate();
        return view('friend.request',compact('friends'));

    }

    public function store(User $user){
        // add friend
        $friend=Friend::firstOrCreate([ 'user_id'=>auth()->id(), 'friend_id'=>$user->id,'status'=>0 ]);
        return back();
    }

    public function update(Friend $friend,$action){
        if ($action===2){
            $friend->delete();
        }else{
            $friend->update([ 'status'=>$action ]);
        }
        return back();
    }

}
