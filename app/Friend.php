<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Friend extends Model{
    protected $guarded=[''];

    public function user(){ return $this->belongsTo(User::class,'user_id'); }
    public function friend(){ return $this->belongsTo(User::class,'friend_id'); }

    public function scopeIsFriend($query, $id){
        return $query->where([['user_id',$id], ['friend_id','=',auth()->id()]])
            ->orWhere([['user_id',auth()->id()],['friend_id',$id]])->where('status',1);
    }
    public function scopeHasRequest($query, $id){
        return $query->where([['user_id',$id], ['friend_id','=',auth()->id()]])
            ->orWhere([['user_id',auth()->id()],['friend_id',$id]])->where('status',0);
    }
}
