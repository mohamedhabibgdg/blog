<?php

namespace App;

use Illuminate\Database\Eloquent\Relations\Pivot;

class ArticleTag extends Pivot {
    protected $guarded=[''];

    protected $table='article_tag';
    public function tag(){ return $this->belongsTo(Tag::class); }
    public function article(){ return $this->belongsTo(Article::class); }
}
