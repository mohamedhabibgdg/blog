<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model{
    protected $perPage=20;

    protected $guarded=[''];
    public function tags(){
        return $this->belongsToMany(Tag::class,'article_tag','article_id', 'tag_id')
        ->withTimestamps()
        ->using(ArticleTag::class)
        ; }
    public function comments(){ return $this->morphMany(Comment::class ,'commentable'); }

    protected static function boot(){
        parent::boot();
        static::creating(function ($article) {
            $article->user_id= $article->user_id ??  auth()->id();
        });
        static::deleting(function ($article) {
            $article->tags()->delete();
            $article->comments()->delete();
        });
    }

}
