@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            @if ($friends->count()>0)
                @foreach ($friends as $friend)
                    <div class="col-md-3 p-3">
                        <div class="card">
                            <div class="card-header">{{$friend->user->name}}</div>
                            <div class="card-body">
                                <form method="post" action="{{route('friends.response',[$friend,1])}}">
                                    @csrf
                                    <button type="submit" class="btn btn-success">Accept</button>
                                </form>
                                <form method="post" action="{{route('friends.response',[$friend,2])}}">
                                    @csrf
                                    <button type="submit" class="btn btn-danger">Refuse</button>
                                </form>
                            </div>
                        </div>
                    </div>
                @endforeach
                <div class="col-md-12">
                    {{$friends->links()}}
                </div>
            @else
                No
            @endif


        </div>
    </div>
@endsection
