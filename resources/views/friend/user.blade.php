@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">

            <div class="col-md-10 p-3">
                <div class="card">
                    <div class="card-header">{{$user->name}}</div>

                    <div class="card-body">
                        {{$user->email}}
                    </div>
                </div>
            </div>

                <div class="col-md-2 p-3">
                    <div class="card">
                        <div class="card-body">
                            @if ($isFriend)
                                @if (!$hasRequest)
                                    <form method="post" action="{{route('friends.request',$user)}}">
                                        @csrf
                                        <button type="submit" class="btn btn-primary">Add Friend</button>
                                    </form>
                                @else
                                    Has Request
                                @endif
                            @else
                                <form method="post" action="{{route('friends.response',[\App\Friend::IsFriend($user->id) ->first(),2])}}">
                                    @csrf
                                    <button type="submit" class="btn btn-danger">Remove Friend</button>
                                </form>
                                Your are Friends
                            @endif
                        </div>
                    </div>
                </div>


        </div>
    </div>
@endsection
