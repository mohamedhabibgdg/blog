@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        @foreach ($users as $user)
            <div class="col-md-3 p-3">
                <div class="card">
                    <div class="card-header">{{$user->name}}</div>
                    <div class="card-body">
                        {{$user->email}}
                    </div>
                </div>
            </div>
        @endforeach
        <div class="col-md-12">
            {{$users->links()}}
        </div>

    </div>
</div>
@endsection
