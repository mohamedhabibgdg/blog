@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Article</div>

                <div class="card-body">
                    <form method="post" action="{{route('articles.store')}}">
                        @csrf
                        <div class="form-group">
                            <label for="title">title</label>
                            <input type="text"
                                   class="form-control" name="title" id="title" aria-describedby="helpId" placeholder="title">
                            @error('title')
                            <small id="helpId" class="form-text text-muted">{{$message}}</small>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="body">body</label>
                            <textarea type="text"
                                   class="form-control" name="body" id="body" aria-describedby="helpId" placeholder="body"></textarea>
                            @error('body')
                            <small id="helpId" class="form-text text-muted">{{$message}}</small>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="tags">tags</label>
                            <input type="text" class="form-control mt-2" name="tags[]" id="tags" aria-describedby="helpId" placeholder="">
                            <input type="text" class="form-control mt-2" name="tags[]" id="tags" aria-describedby="helpId" placeholder="">
                            @error('tags')
                            <small id="helpId" class="form-text text-muted">{{$message}}</small>
                            @enderror
                        </div>
                        <button type="submit" class="btn btn-primary">Create</button>
                    </form>
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
