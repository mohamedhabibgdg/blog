@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{$article->title}}</div>

                <div class="card-body">
                    {!! $article->body!!}
                </div>
            </div>
            <div class="card mt-2">
                <div class="card-body">
                    <h4 class="card-title">comment</h4>
                    @auth
                    <form method="post" action="{{route('articles.create.comment',$article)}}">
                        @csrf
                        <div class="form-group">
                            <label for=""></label>
                            <textarea class="form-control" name="comment" id="" rows="3"></textarea>
                            @error('comment')
                                <p class="text-danger">
                                    {{$message}}
                                </p>
                            @enderror
                        </div>
                        <button type="submit" class="btn btn-primary">comment</button>
                    </form>
                    @else
                        <div class="alert alert-info" role="alert">
                            <strong>you need to login</strong>
                        </div>
                    @endauth
                </div>
            </div>
        </div>
        @foreach ($comments=$article->comments()->whereNull('comment_id')->paginate() as $comment)
            <div class="col-md-8 mt-3">
                <div class="card">
                    <div class="card-header">{{$comment->user->name}}</div>

                    <div class="card-body">
                        {!! $comment->comment !!}
                        <hr>
                        <ul>
                        @foreach ($comment->childs as $child)
                            <li>{{$child->comment}}</li>
                        @endforeach
                        </ul>
                    </div>
                </div>
                @auth
                <div class="card mt-2">
                    <div class="card-body">
                        <h4 class="card-title">Reply</h4>
                        <form method="post" action="{{route('articles.create.comment',['article'=>$article,'comment'=>$comment])}}">
                            @csrf
                            <div class="form-group">
                                <label for=""></label>
                                <textarea class="form-control" name="comment" id="" rows="3"></textarea>
                                @error('comment')
                                <p class="text-danger">
                                    {{$message}}
                                </p>
                                @enderror
                            </div>
                            <button type="submit" class="btn btn-primary">Reply</button>
                        </form>
                    </div>
                </div>
                @else
                    <div class="alert alert-info" role="alert">
                        <strong>you need to login</strong>
                    </div>
                @endauth
            </div>
        @endforeach
        <div class="col-md-12">
            {{$comments->links()}}
        </div>

    </div>
</div>
@endsection
