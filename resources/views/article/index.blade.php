@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        @foreach ($articles as $article)
            <div class="col-md-8 mb-2">
                <div class="card">
                    <div class="card-header"><a href="{{route('articles.show',$article)}}">{{ $article->title }}</a></div>
                    <div class="card-body">
                        {!! $article->body !!}
                    </div>
                </div>
            </div>
        @endforeach
       <div class="col-md-12">
           {{$articles->links()}}
       </div>
    </div>
</div>
@endsection
